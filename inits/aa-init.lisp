(in-package :lem)


(sb-ext:unlock-package :lem)

(in-package :lem)

;; Add Arrows to Lem :)
(use-package :arrow-macros)

;; Use srfi-236 on lem
(use-package :srfi-236)

(sb-ext:lock-package :lem)

;; Handy macro to add keys
(defmacro define-keys (keymap &body bindings)
  `(progn ,@(mapcar
             (lambda (binding)
               `(define-key ,keymap
                  ,(first binding)
                  ,(second binding)))
             bindings)))
