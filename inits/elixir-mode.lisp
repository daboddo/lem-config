(in-package :lem)


;; Elixir configuration
(defvar *fer/elixir-lsp-path*
  '("sh" "/home/fermin/Programming/elixir-lsp/language_server.sh"))

#+sbcl
(setf (lem-lsp-mode/spec:spec-command
       (lem-lsp-mode/spec:get-language-spec 'lem-elixir-mode:elixir-mode))
      *fer/elixir-lsp-path*)
