(in-package :lem)

;; Load vi-mode
(lem-vi-mode:vi-mode)

;; Load main theme
(load-theme "chalk")

;; Set the default package to LEM
(lem-lisp-mode/internal::lisp-set-package "LEM")

;; Load paredit
(lem-paredit-mode:paredit-mode)

(define-key lem-paredit-mode:*paredit-mode-keymap* "C-x p l" 'lem-paredit-mode:paredit-slurp)
(define-key lem-paredit-mode:*paredit-mode-keymap* "C-x p r" 'lem-paredit-mode:paredit-raise)

(add-hook lem:*find-file-hook*
          (lambda (buffer)
            (change-buffer-mode buffer 'lem-paredit-mode:paredit-mode t)))


;; Grep
(defvar *rgrep-command* "ag -Q -U -W 300 -m 1000 --vimgrep ")

(define-command rgrep () ()
  (let* ((default (symbol-string-at-point (lem:current-point)))
         (search (prompt-for-string "Grep: " :initial-value default))
         (directory (prompt-for-directory "Directory: "
                                          :directory (buffer-directory))))
    (lem/grep:grep (str:concat *rgrep-command* search) directory)))


(when (string-equal (str:trim
                     (uiop:run-program "hostnamectl status | grep Chassis | cut -f2 -d \":\" | tr -d ' '" :output :string))
               "laptop " )
  (lem-modeline-battery:enable))

(defparameter lem/filer::*right-pointing-triangle* "> ")
(defparameter lem/filer::*down-pointing-triangle* "\\ ")


;; Global keymap

(lem::define-keys *global-keymap*
  ("C-h f" 'fer/lisp-describe-symbol)
  ("C-h v" 'fer/lisp-describe-symbol)
  ("C-h k" 'describe-key)
  ("C-x 4 0" 'kill-buffer-and-window)
  ("C-x d" 'find-file)
  ("C-x C-l" 'david/open-switch-filer)
  ("C-x l" 'david/toggle-filer)
  ("C-x C-b" 'select-buffer)
  ("C-@" 'execute-command)
  ("C-Space" 'execute-command)
  ("C-c u" 'lem-shell-mode::run-shell)
  ("C-c c" 'fer/clear-overlays)
  ("C-c C-l" 'lem/filer::filer)

  ("C-c p n" 'lem/frame-multiplexer::frame-multiplexer-next)
  ("C-c p p" 'lem/frame-multiplexer::frame-multiplexer-prev)
  ("C-c p c" 'lem/frame-multiplexer::frame-multiplexer-create-with-new-buffer-list)
  ("C-c p d" 'lem/frame-multiplexer::frame-multiplexer-delete))
