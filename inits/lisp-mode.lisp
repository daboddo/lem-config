(in-package :lem)

(define-command fer/lisp-indent-lisp-reference () ()
  (lem/detective::check-change :force t)
  (with-point ((p (current-point)))
    (alexandria:when-let ((reference (lem/detective::current-reference
                                      :point p)))
      (move-to-line p (line-number-at-point
                       (lem/detective:reference-point reference)))
      (move-to-column p 0)
      (with-point ((end p :right-inserting))
        (when (form-offset end 1)
          (indent-points p end))))))

(add-hook lem-lisp-mode/internal:*lisp-repl-mode-hook*
          (lambda ()
            (change-buffer-mode (current-buffer)
                                'lem-paredit-mode:paredit-mode t)))

;; Lisp critic https://github.com/g000001/lisp-critic

(define-command fer/critic-reference () ()
  (let ((beg nil)
        (end nil)
        (output-string nil))
    (lem:with-point ((p (current-point)))
      (funcall
       (variable-value 'lem/language-mode:beginning-of-defun-function :buffer)
       p 1)
      (setf beg (copy-point p)
            end (lem-vi-mode/commands::vi-forward-matching-paren
                 (current-window) p)
            output-string (with-output-to-string (s)
                            (let ((*standard-output* s))
                              (lisp-critic:critique-definition
                               (read-from-string
                                (format nil "~a)"(points-to-string beg end)))))))
      (fer/describe-thing
       (or (and (not (str:emptyp output-string)) output-string)
           "All is well!")))))


;; Use old describe for symbols
(defun fer/describe-thing (string)
  (let ((buffer (make-buffer "*description*")))
    (with-pop-up-typeout-window (stream buffer :erase t)
      (princ string stream))))

(defun show-description (string &key (buffer-name))
  (let ((buffer (make-buffer "*lisp-description*")))
    (change-buffer-mode buffer 'lem-lisp-mode::lisp-mode)
    (with-pop-up-typeout-window (stream buffer :erase t)
      (princ string stream))))

(defun lisp-eval-describe (form)
  (lem-lisp-mode/internal::lisp-eval-async form #'lem::show-description))


(define-command fer/lisp-describe-symbol () ()
  (lem-lisp-mode/internal::check-connection)
  (let ((symbol-name
          (lem-lisp-mode/internal::prompt-for-symbol-name
           "Describe symbol: "
           (or (symbol-string-at-point (current-point)) ""))))
    (when (string= "" symbol-name)
      (editor-error "No symbol given"))
    (lisp-eval-describe `(micros:describe-symbol ,symbol-name))))

;; Major mode keymap

(lem::define-keys lem-lisp-mode:*lisp-mode-keymap*
  ("C-j" 'lem-lisp-mode/eval::lisp-eval-last-expression-and-insert)
  ("C-c i" 'fer/lisp-indent-lisp-reference))

;; Listener keymap

(lem::define-keys lem/listener-mode:*listener-mode-keymap*
  ("C-p" 'lem/listener-mode::listener-previous-input)
  ("C-n" 'lem/listener-mode::listener-next-input)
  ("C-r" 'lem/listener-mode::listener-isearch-history))

(define-key lem-lisp-mode::*lisp-inspector-keymap*
  "C-o" 'lem-lisp-mode::lisp-inspector-pop)
