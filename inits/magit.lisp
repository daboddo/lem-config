(in-package :lem)

(define-command fer/launch-emacs-daemon () ()
  (uiop:run-program '("emacs" "--daemon"))
  (uiop:run-program '("emacsclient" "--eval"
                      "(progn (require 'lemmington) (lemmington-start-server) (lemmington-export-functions))"))
  (lem-elisp-mode/rpc:connect-to-server))

(define-command fer/kill-emacs-daemon () ()
  (uiop:run-program "emacsclient --eval '(kill-emacs)'"))

(define-command magit-status () ()
  (bt:make-thread
   (lambda ()
     (uiop:run-program
      (format nil "/bin/bash ~alaunch-magit.sh ~a"
              (lem-home)
              (buffer-directory))))))

(define-key *global-keymap* "C-x g" 'magit-status)
