(in-package :lem)

(defun %fer/simple-goto (symbol-string)
  (-> symbol-string
    lem-lisp-mode/internal::find-definitions-by-name
    lem-lisp-mode/internal::display-xref-locations))

(define-command fer/maybe-up-directory () ()
  (if (string-equal (lem:mode-name (lem:buffer-major-mode (lem:current-buffer)))
                    "Directory")
      (lem/directory-mode::directory-mode-up-directory)
      (lem-vi-mode/commands:vi-previous-line)))

(define-command david/maybe-filer-enter () ()
  (if (string-equal (lem:mode-name (lem:buffer-major-mode (lem:current-buffer)))
                    "Filer")
      (lem/filer::filer-select)
      (lem-vi-mode/commands:vi-return)))

(define-command david/maybe-filer-space () ()
  (if (string-equal (lem:mode-name (lem:buffer-major-mode (lem:current-buffer)))
                    "Filer")
      (lem/filer::filer-select)
      (lem-vi-mode/commands:vi-forward-char)))

(define-command david/maybe-filer-tab () ()
  (if (string-equal (lem:mode-name (lem:buffer-major-mode (lem:current-buffer)))
                    "Filer")
      (lem/filer::filer-select)
      (lem-vi-mode/commands:vi-jump-next)))

(define-command david/open-switch-filer () ()
  (if (lem/filer::filer-active-p)
      (switch-to-window (lem-core::frame-leftside-window
                         (current-frame)))
      (progn
        (lem/filer::filer)
        (switch-to-window (lem-core::frame-leftside-window
                           (current-frame))))))

(define-command david/toggle-filer () ()
  (if (lem/filer::filer-active-p)
      (lem/filer::filer)
      (progn
        (lem/filer::filer)
        (switch-to-window (lem-core::frame-leftside-window
                           (current-frame))))))

(define-command david/goto-and-recenter () ()
  (lem/thingatp::open-at-point)
  (lem-vi-mode/visual::recenter nil))

(define-command david/up-directory () ()
  (let* ((pwindow (lem/prompt-window::current-prompt-window))
         (wstring (and pwindow (lem/prompt-window::get-input-string))))
    (when wstring
      (lem/prompt-window::replace-prompt-input 
       (ignore-errors
         (subseq wstring 0
                 (1+ (position
                      #\/
                      (str:trim-right wstring :char-bag '(#\/ ))
                      :from-end t :test #'char-equal))))))))

(define-key lem/prompt-window::*prompt-mode-keymap* "C-l" 'david/up-directory)

(define-command fer/goto-major-mode () ()
  (let ((current-mode (lem:buffer-major-mode (lem:current-buffer))))
    (%fer/simple-goto
     (format nil "~a::~a"
             (package-name (symbol-package current-mode))
             (symbol-name current-mode)))))

(define-command david/goto-key-definition () ()
  (show-message "goto-key: ")
  (redraw-display)
  (let* ((kseq (read-key-sequence))
         (keybind (find-keybind kseq)))
    (%fer/simple-goto
     (format nil "~a::~a"
             (package-name (symbol-package keybind))
             (symbol-name keybind))))
  (lem-vi-mode/visual::recenter nil))

(define-command kill-buffer-and-window () ()
  (kill-buffer (current-buffer))
  (delete-window (current-window)))

(define-command open-init-file () ()
  (-> (lem-home)
    (merge-pathnames "inits")
    find-file))

(define-command pwd () ()
  (let ((lem-core::*message-timeout* 3))
    (message (buffer-filename))))

(define-command fer/clear-overlays () ()
  (clear-overlays (current-buffer)))
