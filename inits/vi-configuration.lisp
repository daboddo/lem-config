(in-package :lem)

(define-command emacs-state () ()
  (if (string-equal "vi"
                    (mode-name (lem-core::current-global-mode)))
      (lem-core::emacs-mode)
      (lem-vi-mode:vi-mode)))

(defparameter *fer/vi-exit-q-buffers* '("*peek-source*" "*lisp-inspector*"))

(define-command fer/exit-vi-buffer ()()
  (let* ((buffer (current-buffer))
         (buffer-name (buffer-name buffer)))
    (when (find buffer-name *fer/vi-exit-q-buffers*
                :test 'string-equal)
      (delete-active-window))))


;; Motion keymap
(lem::define-keys lem-vi-mode:*motion-keymap*
  ("g d" 'david/goto-and-recenter)
  ("C-t" 'lem/go-back:go-back-global)
  ("C-b" 'lem-vi-mode/commands:vi-backward-char)
  ("C-f" 'lem-vi-mode/commands:vi-forward-char)

  ("q" 'fer/exit-vi-buffer)
  ("-" 'fer/maybe-up-directory)
  ("Return" 'david/maybe-filer-enter)
  ("Space" 'david/maybe-filer-space)
  ("Tab" 'david/maybe-filer-tab)


  ("C-n" 'lem/detective::detective-next)

  ("Shift-Tab" 'lem/abbrev::abbrev-with-pop-up-window))

;; Global keymap
(define-key *global-keymap* "C-x u" 'lem-vi-mode/binds::vi-undo)

(define-key *global-keymap* "C-z" 'emacs-state)
