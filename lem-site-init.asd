;; don't edit !!!
(asdf/parse-defsystem:defsystem "lem-site-init"
  :depends-on
  (:lem-modeline-battery :lem-trailing-spaces :lisp-critic :arrow-macros
   :srfi-236)
  :components
  ((:file "inits/aa-init") (:file "inits/elixir-mode") (:file "inits/general")
   (:file "inits/js-mode") (:file "inits/lisp-mode") (:file "inits/magit")
   (:file "inits/misc-commands") (:file "inits/scheme-mode")
   (:file "inits/vi-configuration")))